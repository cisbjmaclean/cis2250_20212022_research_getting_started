package info.hccis.performancehall_mobileappbasicactivity;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import java.lang.reflect.Array;
import java.util.ArrayList;

import info.hccis.performancehall_mobileappbasicactivity.bo.TicketOrderBO;
import info.hccis.performancehall_mobileappbasicactivity.bo.TicketOrderViewModel;
import info.hccis.performancehall_mobileappbasicactivity.databinding.FragmentViewOrdersBinding;

public class ViewOrdersFragment extends Fragment {

    private FragmentViewOrdersBinding binding;

    //replaced by TicketOrderViewModel
    //    private static ArrayList<TicketOrderBO> orders = new ArrayList();
    //    private static double  total = 0;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        binding = FragmentViewOrdersBinding.inflate(inflater, container, false);
        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TicketOrderViewModel ticketOrderViewModel = new ViewModelProvider(getActivity()).get(TicketOrderViewModel.class);

        //Bundle is accessed to get the ticket order which is passed from the add order fragment.
        Bundle bundle = getArguments();
        TicketOrderBO ticketOrderBO = (TicketOrderBO) bundle.getSerializable(AddOrderFragment.KEY);
        Log.d("ViewOrdersFragment BJM", "Ticket passed in:  "+ticketOrderBO.toString());

//        orders.add(ticketOrderBO);
//        total += ticketOrderBO.calculateTicketPrice();

        //Build the output to be displayed in the textview
        String output = "";
        double total = 0;
        for (TicketOrderBO order: ticketOrderViewModel.getTicketOrders()){
            output += order.toString()+System.lineSeparator();
            total += order.calculateTicketPrice();
        }
        output += System.lineSeparator()+"Total: $"+total;

        binding.textviewOrderDetails.setText(output);



        //Button sends the user back to the add fragment
        binding.buttonAddOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(ViewOrdersFragment.this)
                        .navigate(R.id.action_ViewOrdersFragment_to_AddOrderFragment);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}