package com.example.helloworld20220107;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.nio.file.Files;

public class MainActivity extends AppCompatActivity {

    EditText editTextHollPassNumber;
    EditText editTextNumberOfTickets;
    Button buttonCalculate;
    TextView textViewCost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(MainActivity.class.getName(),"BJM-Message from the onCreate method" );
        setContentView(R.layout.activity_main);

        editTextHollPassNumber = (EditText) findViewById(R.id.editTextHollpassNumber);

        buttonCalculate = (Button) findViewById(R.id.buttonCalculate);
        buttonCalculate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Do something in response to button click
                Log.d("BJM-MainActivity","HollPass Number entered ="+editTextHollPassNumber.getText().toString());
                Log.d("BJM-MainActivity", "Calculate button was clicked.");
            }
        });

    }




    @Override
    protected void onStart() {
        Log.d("BJM-MainActivity","onStart was triggered");
        super.onStart();
    }

    @Override
    protected void onRestart() {
        Log.d("BJM","onRestart was triggered");
        super.onRestart();
    }

    @Override
    protected void onStop() {
        Log.d("BJM","onStop was triggered");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.d("BJM","onDestroy was triggered");
        super.onDestroy();
    }
}