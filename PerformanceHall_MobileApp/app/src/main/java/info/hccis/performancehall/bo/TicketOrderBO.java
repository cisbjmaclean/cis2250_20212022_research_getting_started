package info.hccis.performancehall.bo;


/**
 * Business logic class for ticket purchaes
 *
 * @author bjmaclean
 * @since 20220105
 */
public class TicketOrderBO {

    public static final double COST_TICKET =10;
    public static final double DISCOUNT_HOLLPASS = 0.1;
    public static final double DISCOUNT_VOLUME_10 = 0.1;
    public static final double DISCOUNT_VOLUME_20 = 0.15;

    /**
     * Calculate the cost for a ticket order
     *
     * @param numberOfTickets
     * @param hasHollPass
     * @return cost
     * @since 20220105
     * @author cis2250
     */
    public double calculateTicketPrice(int numberOfTickets, boolean hasHollPass) {
        double discount = 0;
        if(numberOfTickets >=20){
            discount += DISCOUNT_VOLUME_20;
        }else if(numberOfTickets >=10){
            discount += DISCOUNT_VOLUME_10;
        }

        if(hasHollPass){
            discount += DISCOUNT_HOLLPASS;
        }

        return numberOfTickets * COST_TICKET * (1-discount);
    }

    /**
     * Verify if a HollPassNumber if correct
     * Rules:  Length is 5 and a multiple of 13
     *
     * @param hasHollPass
     * @return valid result
     * @since 20220105
     * @author cis2250
     */
    public boolean validateHollPassNumber(int hollPassNumber) {

        boolean valid = false;

        if(hollPassNumber >= 10000 && hollPassNumber < 100000){
            if(hollPassNumber % 13 == 0){
                valid=true;
            }
        }

        return valid;
    }
}
