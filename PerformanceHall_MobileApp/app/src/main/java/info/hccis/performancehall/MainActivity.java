package info.hccis.performancehall;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.nio.file.Files;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import info.hccis.performancehall.bo.TicketOrderBO;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_COST = "info.hccis.performancehall.cost";
    EditText editTextHollPassNumber;
    EditText editTextNumberOfTickets;
    Button buttonCalculate;
    TextView textViewCost;

    /**
     * The oncreate method runs when Android is creating this activity.  Remember this Activity is
     * listed in the Android Manifest file as the launcher activity.
     *
     * @param savedInstanceState
     * @author CIS2250
     * @since 20220111
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(MainActivity.class.getName(), "BJM-Message from the onCreate method");
        setContentView(R.layout.activity_main);

        editTextHollPassNumber = (EditText) findViewById(R.id.editTextHollpassNumber);
        editTextNumberOfTickets = (EditText) findViewById(R.id.editTextNumberOfTickets);
        textViewCost = (TextView) findViewById(R.id.textViewCost);
        buttonCalculate = (Button) findViewById(R.id.buttonCalculate);

        buttonCalculate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Do something in response to button click
                Log.d("BJM-MainActivity", "HollPass Number entered =" + editTextHollPassNumber.getText().toString());
                Log.d("BJM-MainActivity", "Number of tickets = " + editTextNumberOfTickets.getText().toString());
                Log.d("BJM-MainActivity", "Calculate button was clicked.");


                TicketOrderBO ticketOrderBO = new TicketOrderBO();
                int hollPassNumber = 0;
                boolean validHollPassNumber;
                try {
                    hollPassNumber = Integer.parseInt(editTextHollPassNumber.getText().toString());
                    validHollPassNumber= ticketOrderBO.validateHollPassNumber(hollPassNumber);
                } catch (Exception e) {
                    hollPassNumber = 0;
                    validHollPassNumber=false;
                }

                try {
                    int numberOfTickets = Integer.parseInt(editTextNumberOfTickets.getText().toString());
                    if (numberOfTickets > 100) {
                        throw new Exception("Invalid Number of tickets");
                    }
                    double cost = ticketOrderBO.calculateTicketPrice(numberOfTickets, validHollPassNumber);
                    //Now that I have the cost, want to set the value on the textview.
                    Locale locale = new Locale("en", "CA");
                    DecimalFormat decimalFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(locale);
                    DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance(locale);
                    decimalFormat.setDecimalFormatSymbols(dfs);
                    String formattedCost = decimalFormat.format(cost);
                    textViewCost.setText(formattedCost);
                    sendToViewOrders(cost);
                }catch(NumberFormatException nfe){
                    editTextNumberOfTickets.setText("");
                    textViewCost.setText("Invalid number of tickets");
                }catch(Exception e){
                    editTextNumberOfTickets.setText("");
                    textViewCost.setText("Maximum number of tickets is 100");
                }
            }
        });
    }

    /**
     * Send the user to the view orders activity
     * @since 20220114
     * @author BJM
     *
     */
    public void sendToViewOrders(double cost) {
        Intent intent = new Intent(this, ViewOrdersActivity.class);
        intent.putExtra(EXTRA_COST, cost);
        startActivity(intent);
    }

    @Override
    protected void onStart() {
        Log.d("BJM-MainActivity", "onStart was triggered");
        super.onStart();
    }

    @Override
    protected void onRestart() {
        Log.d("BJM", "onRestart was triggered");
        super.onRestart();
    }

    @Override
    protected void onStop() {
        Log.d("BJM", "onStop was triggered");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.d("BJM", "onDestroy was triggered");
        super.onDestroy();
    }
}