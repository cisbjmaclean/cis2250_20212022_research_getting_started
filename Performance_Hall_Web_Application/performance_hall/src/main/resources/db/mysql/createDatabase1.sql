DROP DATABASE IF EXISTS cis2250_performance_hall;
CREATE DATABASE cis2250_performance_hall;
use cis2250_performance_hall;

CREATE TABLE CodeType (codeTypeId int(3) COMMENT 'This is the primary key for code types',
  englishDescription varchar(100) NOT NULL COMMENT 'English description',
  frenchDescription varchar(100) DEFAULT NULL COMMENT 'French description',
  createdDateTime datetime DEFAULT NULL,
  createdUserId varchar(20) DEFAULT NULL,
  updatedDateTime datetime DEFAULT NULL,
  updatedUserId varchar(20) DEFAULT NULL
) COMMENT 'This tables holds the code types that are available for the application';

INSERT INTO CodeType (CodeTypeId, englishDescription, frenchDescription, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
(1, 'User Types', 'User Types FR', sysdate(), '', CURRENT_TIMESTAMP, '');

CREATE TABLE CodeValue (
  codeTypeId int(3) NOT NULL COMMENT 'see code_type table',
  codeValueSequence int(3) NOT NULL,
  englishDescription varchar(100) NOT NULL COMMENT 'English description',
  englishDescriptionShort varchar(20) NOT NULL COMMENT 'English abbreviation for description',
  frenchDescription varchar(100) DEFAULT NULL COMMENT 'French description',
  frenchDescriptionShort varchar(20) DEFAULT NULL COMMENT 'French abbreviation for description',
  sortOrder int(3) DEFAULT NULL COMMENT 'Sort order if applicable',
  createdDateTime datetime DEFAULT NULL,
  createdUserId varchar(20) DEFAULT NULL,
  updatedDateTime datetime DEFAULT NULL,
  updatedUserId varchar(20) DEFAULT NULL
) COMMENT='This will hold code values for the application.';

INSERT INTO CodeValue (codeTypeId, codeValueSequence, englishDescription, englishDescriptionShort, frenchDescription, frenchDescriptionShort, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
(1, 1, 'General', 'General', 'GeneralFR', 'GeneralFR', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin');
INSERT INTO CodeValue (codeTypeId, codeValueSequence, englishDescription, englishDescriptionShort, frenchDescription, frenchDescriptionShort, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
(1, 2, 'Admin', 'Admin', 'Admin', 'Admin', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin');


CREATE TABLE UserAccess (
  userAccessId int(3) NOT NULL,
  username varchar(100) NOT NULL COMMENT 'Unique user name for app',
  password varchar(128) NOT NULL,
  name varchar(128),
  userAccessStatusCode int(3) NOT NULL DEFAULT '1' COMMENT 'Code type #2',
  userTypeCode int(3) NOT NULL DEFAULT '1' COMMENT 'Code type #1',
  createdDateTime datetime DEFAULT NULL COMMENT 'When user was created.'
);

ALTER TABLE CodeType
  ADD PRIMARY KEY (codeTypeId);

ALTER TABLE CodeValue
  ADD PRIMARY KEY (codeTypeId,codeValueSequence);
--  ADD KEY codeTypeId (codeTypeId);

ALTER TABLE UserAccess
  ADD PRIMARY KEY (userAccessId),
  ADD KEY userTypeCode (userTypeCode);

ALTER TABLE CodeType
  MODIFY codeTypeId int(3) NOT NULL COMMENT 'This is the primary key for code types';

ALTER TABLE CodeValue
  MODIFY codeValueSequence int(3) NOT NULL;

ALTER TABLE UserAccess
  MODIFY userAccessId int(3) NOT NULL AUTO_INCREMENT;



use cis2250_performance_hall;

CREATE TABLE Customer(
id int(5) COMMENT 'This is the primary key',
customerName varchar(40) COMMENT 'Name',
hollpassNumber varchar(5) COMMENT 'HollPass Number',
phoneNumber varchar(10) COMMENT '1112223333',
emailAddress varchar(100) COMMENT 'Email Address'
) COMMENT 'This table holds person data';

ALTER TABLE Customer
  ADD PRIMARY KEY (id);
ALTER TABLE Customer
  MODIFY id int(5) NOT NULL AUTO_INCREMENT COMMENT 'This is the primary key', AUTO_INCREMENT=1;

CREATE TABLE TicketOrder(
id int(5),
numberOfTickets int(4) COMMENT 'Number of Tickets',
costOfTickets decimal(6,2) COMMENT 'Cost of Tickets',
customerId int(4) COMMENT 'Reference to Customer id'
) COMMENT 'This table holds ticket order data';

ALTER TABLE TicketOrder
  ADD PRIMARY KEY (id);
ALTER TABLE TicketOrder
  MODIFY id int(4) NOT NULL AUTO_INCREMENT COMMENT 'This is the primary key', AUTO_INCREMENT=1;

insert into Customer values(0, "John Rogers", 0, "9023146677","jrogers@gmail.com");
insert into Customer values(0, "Will Smith", 13013, "9023146678","wsmith@gmail.com");
insert into Customer values(0, "Harrison Ford", 26026, "9023146679","hford@gmail.com");
insert into Customer values(0, "Brielle Paterson", 0, "9023146680","bpaterson@gmail.com");

insert into TicketOrder values(0, 10, 90.0, 1);
insert into TicketOrder values(0, 10, 80.0, 2);
insert into TicketOrder values(0, 20, 150.0, 2);
insert into TicketOrder values(0, 1, 9.0, 3);
